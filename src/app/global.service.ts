import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GlobalService {
 data:any;
 
  constructor(public http:HttpClient) { }

  retrieveData(){
    return new Promise(resolve => {
      this.http.post('http://192.168.2.22/Aud/AudDiary/gps.php', this.data)
      .subscribe(data => {
        this.data = data;
        resolve(data);
      });
    });
}  

getData(){
  return this.data;
}
}
