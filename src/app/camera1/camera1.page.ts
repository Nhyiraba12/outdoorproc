import { Component, OnInit,ChangeDetectorRef,ViewChild,ElementRef } from '@angular/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import {Router,ActivatedRoute} from '@angular/router';
import {ActionSheetController,ToastController,Platform} from '@ionic/angular';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {AlertController} from '@ionic/angular';
import {LoadingController} from '@ionic/angular';
import {Camera,CameraOptions,PictureSourceType} from '@ionic-native/Camera/ngx';
import {File,FileEntry} from '@ionic-native/File/ngx';
// import {HttpClient} from '@angular/common/http';
import {WebView} from '@ionic-native/ionic-webview/ngx';
import {Storage} from '@ionic/storage';
import { finalize } from 'rxjs/operators';
import {HttpHandler} from '@angular/common/http';
import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import * as watermark from 'watermarkjs';
import { Capacitor, CameraResultType} from '@capacitor/core';
import { TypeaheadService } from '../services/typeahead.service';

const STORAGE_KEY = 'xav_images';
const store_key = 'coords';
// const uploadURL = 'http://192.168.2.33/Aud/AudDiary/upload.php'; 
// const upploadURL = 'http://127.0.0.1/Aud/AudDiary/upload.php';
@Component({
  selector: 'app-camera',
  templateUrl: './camera1.page.html',
  styleUrls: ['./camera1.page.scss'],
})
export class Camera1Page implements OnInit {
 @ViewChild("gps_id") gps_id;
 private result;
// @ViewChild("longitude") longitude;
// @ViewChild("latitude") latitude;
// @ViewChild("accuracy") accuracy;
// @ViewChild("timestamp") timestamp;
@ViewChild("upclose") upclose;
@ViewChild("id") id;
//Call objects
locationCoords:any;
timetest:any;
 gpsid = this.tservice.getIdd().subscribe(data=>{
          console.log(data);
          this.result = data;
        
          // console.log(this.result);
        });
images=[];

  disableButton;

  // @ViewChild("id")id;
  @ViewChild("longitude") longitude;
  @ViewChild("latitude") latitude;
  @ViewChild("accuracy") accuracy;
  @ViewChild("timestamp") timestamp;

   // @ViewChild("fifty") fifty;
  //Call objects
 
  
  constructor(private loadingctrl:LoadingController,private platform:Platform,private tservice:TypeaheadService,private transfer:FileTransfer,private router:Router,private filePath:FilePath,private locationAccuracy:LocationAccuracy,private androidPermissions:AndroidPermissions,private geolocation: Geolocation,private webview:WebView,private alertCtrl:AlertController,private actionsheet:ActionSheetController,private plat:Platform,private storage:Storage,private file:File,private http:Http,private toastCtrl:ToastController,private loading:LoadingController,private camera:Camera,private ref:ChangeDetectorRef) { 

//declare values
    //  this.locationCoords = {
    //   latitude: "",
    //   longitude: "",
    //   accuracy: "",
    //   timestamp: ""
    // }
    // this.timetest = Date.now();

     // this.locationCoords = {
     //    longitude: "",
     //    latitude: "",
     //    accuracy: "",
     //    timestamp: ""
     //  }
     //  this.timetest = Date.now();

     //  this.platform.ready().then(() => {
     //  });
  }

 

  ngOnInit() {
   this.plat.ready().then(()=>{
      this.loadStoredImages();
    });

//     let options = {

//          quality: 100,
       
//         saveToPhotoAlbum: true,
//        correctOrientation: true
//           };

//     this.camera.getPicture(options).then((imagePath) => {
//      // imageData is either a base64 encoded string or a file URI
//      // If it's base64:
    
//    const fileTransfer: FileTransferObject = this.transfer.create();
//    let temp_image =new Date().getTime()+'.'+ this.result+'.jpg';
//     let options1: FileUploadOptions = {
//        fileKey: 'file',
//        fileName:temp_image,
//        headers: {}
    
//     }
//   const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
//     const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
// this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
// // fileTransfer.upload(imagePath, 'http://127.0.0.1/Aud/AudDiary/up.php', options1)
// //  .then((data) => {
// //    // success
// //    alert("success");
// //  }, (err) => {
// //    // error
// //    alert("error"+JSON.stringify(err));
// //  });
//  fileTransfer.upload(imagePath, 'http://192.168.2.22/Aud/AudDiary/up.php', options1)
//  .then((data) => {
//    // success
//    // alert("Saved");
//  }, (err) => {
//    // error
//    alert("error"+JSON.stringify(err));
//  });


//   });

     


// // //Check GPS Permission
// // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
// //       result => {
// //         if (result.hasPermission) {

// //           //If having permission show 'Turn On GPS' dialogue
// //           this.askToTurnOnGPS();
// //         } else {

// //           //If not having permission ask for permission
// //           this.requestGPSPermission();
// //         }
// //       },
// //       err => {
// //         alert(err);
// //       }

// //     );
// console.log(history.state.res);
 // this.disableButton = true;
 }
 //Request permission if none granted
  //  requestGPSPermission() {
  //     this.locationAccuracy.canRequest().then((canRequest: boolean) => {
  //       if (canRequest) {
  //         console.log("4");
  //       } else {
  //         //Show 'GPS Permission Request' dialogue
  //         this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
  //           .then(
  //             () => {
  //               // call method to turn on GPS
  //               this.askToTurnOnGPS();
  //             },
  //             error => {
  //               //Show alert if user click on 'No Thanks'
  //               alert('requestPermission Error requesting location permissions ' + error)
  //             }
  //           );
  //       }
  //     });
  //   }
  // //ask user to turn on
  //   askToTurnOnGPS() {
  //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
  //       () => {
  //         // When GPS Turned ON call method to get Accurate location coordinates
  //         this.getLocationCoordinates()
  //       },
  //       error => alert('Error requesting location permissions ' + JSON.stringify(error))
  //     );
  //   }
  //   // Methods to get device accurate coordinates using device GPS
  //   getLocationCoordinates() {
  //     this.geolocation.getCurrentPosition().then((resp) => {
  //       this.locationCoords.longitude = resp.coords.latitude;
  //       this.locationCoords.latitude = resp.coords.longitude;
  //       this.locationCoords.accuracy = resp.coords.accuracy;
  //       this.locationCoords.timestamp = resp.timestamp;
  //     }).catch((error) => {
  //       alert('Error getting location' + error);
  //     });
  //   }


//load images unto page from storage
 loadStoredImages(){
     this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }

  //stores filePath ,convert to displayable file
   pathForImage(img){
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }


  async presentToast(text){
    const toast= await this.toastCtrl.create({
      message:text,
      position:'bottom',
      duration:3000
    });
    toast.present();
  }
  async select(){
    const sheet = await this.actionsheet.create({
      header:'Get Source Image',
         buttons:[//{
        //text:'Load From Gallery',handler:()=>{this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
         //}
        //},
      {
        text:'Camera',handler:()=>{this.takePhoto(this.camera.PictureSourceType.CAMERA);
      }
    },
    {
      text:'Cancel',
      role:'Cancel'
    }]

    });
    await sheet.present();
  }

  //takePhoto
   takePhoto(sourceType : PictureSourceType){

    //  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
    //     result => {
    //       if (result.hasPermission) {

    //         //If having permission show 'Turn On GPS' dialogue
    //         this.askToTurnOnGPS();
    //       } else {

    //         //If not having permission ask for permission
    //         this.requestGPSPermission();
    //       }
    //     },
    //     err => {
    //       alert(err);
    //     }

    //   );

    // this.disableButton = false;



    //oredy here
 let options = {

        quality: 100,
              sourceType: sourceType,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
           correctOrientation: true
          };

    this.camera.getPicture(options).then((imagePath) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64:

   const fileTransfer: FileTransferObject = this.transfer.create();
   let temp_image =new Date().getTime()+'.'+ this.result+'.jpg';
    let options1: FileUploadOptions = {
       fileKey: 'file',
       fileName:temp_image,
       headers: {}
    
    }
    const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
// fileTransfer.upload(imagePath, 'http://127.0.0.1/Aud/AudDiary/up.php', options1)
//  .then((data) => {
//    // success
//    alert("success");
//  }, (err) => {
//    // error
//    alert("error"+JSON.stringify(err));
//  });
 fileTransfer.upload(imagePath, 'http://166.62.28.93/Aud/AudDiary/up.php', options1)
 .then((data) => {
   // success
   // alert("Saved");
 }, (err) => {
   // error
   alert("error"+JSON.stringify(err));
 });


  });


}


 createFileName(){
    const d = new Date(),
    n = d.getTime(),
    newFileName = n + ".jpg";
    return newFileName;
  }

copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName)
    .then(success => {
        this.updateStoredImages(newFileName);

         this.presentToast('Success while storing file.');
    }, error => {
        this.presentToast('Error while storing file.');
    });
}



  updateStoredImages(name){
   this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }
 
        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);
 
        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };
 
        this.images = [newEntry, ...this.images];  
        this.ref.detectChanges();
    });
}


async Remove(imgEntry,position){
   let confirm =await this.alertCtrl.create({
        message: 'Sure you want to delete this photo? Cannot be undone!!',
        buttons: [
          {
            text: 'No',
            handler: () => {
              console.log('Disagree clicked');
            }
          }, {
            text: 'Yes',
            handler: () => {
              console.log('Agree clicked');
              // this.photos.splice(imgEntry, 1);
              this.images.splice(position,1);
  this.storage.get(STORAGE_KEY).then(images=>{
    let arr = JSON.parse(images);
    let filtered = arr.filter(name=>name != imgEntry.name);
    this.storage.set(STORAGE_KEY,JSON.stringify(filtered));

    var correctPath = imgEntry.filePath.substr(0,imgEntry.filePath.lastIndexOf('/')+1);

    this.file.removeFile(correctPath,imgEntry.name).then(res=>{
      this.presentToast('File deleted');
    });
  });
            }
          }
        ]
      });
    confirm.present();
  
}

//Upload Function
Upload(imgEntry,position){
 this.file.resolveLocalFilesystemUrl(imgEntry.filePath).then(entry => {
            ( <FileEntry>entry).file(file => 
              this.readFile(file,imgEntry,position));
        })
                .catch(err => {
            this.presentToast('Error while reading file.');
        });
}


//read snapped file
readFile(file:any,imgEntry,position){
  const reader = new FileReader();
    reader.onload = () => {
        const formData = new FormData();
        const imgBlob = new Blob([reader.result], {
            type: file.type
        });
        formData.append('file', imgBlob, file.name);
        // this.uploadImageData(formData,imgEntry,position);
    };
    reader.readAsArrayBuffer(file);
  }




// async presentAlert(text){
//     const alert= await this.alertCtrl.create({
//       message:text,
//       buttons: ['OK']
//       }).then(result=>{
//          result.present();
//       });
//   }

// //uploadImageData
//   async uploadImageData(formData:FormData,imgEntry,position){
//    const loading = await this.loadingctrl.create({
//         message: 'Uploading image...',
//     });
//     await loading.present();
 
//     this.http.post('http://127.0.0.1/Aud/AudDiary/upload.php', formData)
//         .pipe(
//             finalize(() => {
//                 loading.dismiss();
//             })
//         )
//         .subscribe(res => {
//             if (res['success']) {
//                 this.presentToast('File upload complete.')
//             } else {
//                 this.presentToast('File upload failed.')
//             }
//         });
//         this.http.post('http://192.168.2.33/Aud/AudDiary/upload.php', formData)
//         .pipe(
//             finalize(() => {
//                 loading.dismiss();
//             })
//         )
//         .subscribe(res => {
//             if (res['success']) {
//                 this.presentToast('File upload complete.')
//             } else {
//                 this.presentToast('File upload failed.')
//             }
//         });
//   }




  async Permission(){
     this.storage.remove(STORAGE_KEY);
 // console.log('longitude:',this.longitude.value);
if(this.gps_id?.value==""){

     let alert = this.alertCtrl.create({

     message:"Field is empty",
     buttons: ['OK']
     }).then(res=>{
       res.present();
     }); }else{
      var headers = new Headers();
        headers.append('Access-Control-Allow-Origin' , '*');
     headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
     headers.append('Accept','application/json');
     headers.append('content-type','application/json');
        let options = new RequestOptions({ headers: headers });


          let data = {
            // longitude: this.longitude?.value,
            // latitude: this.latitude?.value,
            // accuracy: this.accuracy?.value,
            // timestamp: this.timestamp?.value,
            gps_id:this.gps_id?.value
          };

        const loader =await this.loadingctrl.create({
        message: 'Files Saved...Prepare for next session!!',
      });

     loader.present().then(() => {
      //external
    this.http.post('http://166.62.28.93/Aud/AudDiary/update.php',data,options)
      // .map(res => res.json())
      .subscribe(res => {
      console.log(res)
      loader.dismiss()
      // if(res=="Upload Successful"){
       
      //  let alert= this.alertCtrl.create({
      //     message:"Upload Successful",
      //     buttons: ['OK']
      //     }).then(result=>{
      //        result.present();
      //     });
         
          this.router.navigateByUrl('place');
      // }else
      // {
      //  let alert = this.alertCtrl.create({
      //  message:"ERROR",
      //  buttons: ['OK']
      //  }).then(result=>{
      //    result.present();
      //  });
      //   } 
      });
      
 // const loading=await this.loadingctrl.create({
 //      message:'Files saved...Take 50m Pictures',
 //      spinner:'circles',
 //      duration:2200
 //    });
 //    setTimeout(()=>{
 //      this.router.navigateByUrl('selector');
 //    },2200);
 //    await loading.present();
 });
}
}

}

  
