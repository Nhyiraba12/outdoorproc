import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BillboardPage } from './billboard.page';

describe('BillboardPage', () => {
  let component: BillboardPage;
  let fixture: ComponentFixture<BillboardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BillboardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BillboardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
