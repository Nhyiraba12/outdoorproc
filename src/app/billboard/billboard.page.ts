import { Component,OnInit,ViewChild } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {AlertController} from '@ionic/angular';
import {LoadingController} from '@ionic/angular';
import { VerifyPage } from '../pages/verify/verify.page';
import {Http, Headers, RequestOptions}  from "@angular/http";
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-billboard',
  templateUrl: './billboard.page.html',
  styleUrls: ['./billboard.page.scss'],
})
export class BillboardPage implements OnInit {
	@ViewChild("place") place;
	//@ViewChild("name") name;
	data:string;
	items:any;

  constructor(public navCtrl:NavController, private http: Http,private router:Router,private loadingctrl:LoadingController,private alertCtrl:AlertController) { }


  ngOnInit() {
  }

async Adloc(){
 
console.log('location: ',this.place.value);
 if(this.place.value==""){

 let alert = this.alertCtrl.create({

 message:"Field is empty",
 buttons: ['OK']
 }).then(res=>{
 	res.present();
 });
}
 else
 {

  var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });


      let data = {
        place: this.place.value
      };

      

 const loader =await this.loadingctrl.create({
    message: 'Loading.. Wait!!',
  });

 loader.present().then(() => {


 // this.http.post('http://127.0.0.1/Aud/AudDiary/login.php',data,options)
 //  .map(res => res.json())
 //  .subscribe(res => {
 //  console.log(res)
 //  loader.dismiss()
 //  if(res=="Login successful"){
   
 //   let alert= this.alertCtrl.create({
 //      message:"Welcome..Enter Location",
 //      buttons: ['OK']
 //      }).then(result=>{
 //      	 result.present();
 //      });
     
 //      this.router.navigateByUrl('location');
  // }else
  // {
  //  let alert = this.alertCtrl.create({
  //  message:"ERROR",
  //  buttons: ['OK']
  //  }).then(result=>{
  //  	result.present();
  //  });
  //   } 
  // });


//external
this.http.post('https://192.168.2.22/Aud/AudDiary/locate.php',data,options)
  .map(res => res.json())
  .subscribe(res => {
  console.log(res)
  loader.dismiss()
  if(res=="Location Selected"){
   
   let alert= this.alertCtrl.create({
      message:"Welcome..",
      buttons: ['OK']
      }).then(result=>{
      	 result.present();
      });
     
      this.router.navigateByUrl('camera');
  }else
  {
   let alert = this.alertCtrl.create({
   message:"ERROR",
   buttons: ['OK']
   }).then(result=>{
   	result.present();
   });
    } 
  });



  });
 
   }
  
  }



}












	