import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BillboardPageRoutingModule } from './billboard-routing.module';

import { BillboardPage } from './billboard.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BillboardPageRoutingModule
  ],
  declarations: [BillboardPage]
})
export class BillboardPageModule {}
