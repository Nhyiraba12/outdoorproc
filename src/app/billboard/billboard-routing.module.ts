import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillboardPage } from './billboard.page';

const routes: Routes = [
  {
    path: '',
    component: BillboardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BillboardPageRoutingModule {}
