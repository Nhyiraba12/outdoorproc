  import { Component, OnInit,ViewChild } from '@angular/core';
  import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
  import { Geolocation } from '@ionic-native/geolocation/ngx';
  import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
  import {Router,ActivatedRoute} from '@angular/router';
  import {Http, Headers, RequestOptions}  from "@angular/http";
  import {AlertController} from '@ionic/angular';
  import {LoadingController,Platform} from '@ionic/angular';
  // import {Plugins} from '@capacitor/core';
  import {Storage} from '@ionic/storage';
  import { NgForm } from '@angular/forms';
  import { Subscription } from 'rxjs/Subscription';
  import {File,FileEntry} from '@ionic-native/File/ngx';
  import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
  import { FilePath } from '@ionic-native/file-path/ngx';
  import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
  import * as watermark from 'watermarkjs';
  import { Capacitor, CameraResultType} from '@capacitor/core';


  const store_key = 'coords';


  // const {Storage} = Plugins;
  @Component({
    selector: 'app-gps',
    templateUrl: './gps.page.html',
    styleUrls: ['./gps.page.scss'],
  })
  export class GpsPage implements OnInit {
  text:string;
   disableButton;

  @ViewChild("id")id;
  @ViewChild("longitude") longitude;
  @ViewChild("latitude") latitude;
  @ViewChild("accuracy") accuracy;
  @ViewChild("timestamp") timestamp;

   // @ViewChild("fifty") fifty;
  //Call objects
  locationCoords:any;
  timetest:any;
    constructor(private geolocation:Geolocation,private file:File,private platform:Platform,private storage:Storage,private loadingctrl:LoadingController,private alertCtrl:AlertController,private http: Http,private androidPermissions:AndroidPermissions,private locationAccuracy:LocationAccuracy,private router:Router) { 

  //declare values
    	 this.locationCoords = {
        longitude: "",
        latitude: "",
        accuracy: "",
        timestamp: ""
      }
      this.timetest = Date.now();

      this.platform.ready().then(() => {
      });
    }

    ngOnInit() {
    	// this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
     //    result => {
     //      if (result.hasPermission) {

     //        //If having permission show 'Turn On GPS' dialogue
     //        this.askToTurnOnGPS();
     //      } else {

     //        //If not having permission ask for permission
     //        this.requestGPSPermission();
     //      }
     //    },
     //    err => {
     //      alert(err);
     //    }

     //  );
     // this.storage.set('store_key',JSON.stringify(this.locationCoords));
     this.disableButton = true;
    
    }

  //Check if permission has been granted
  checkGPSPermission(){
  	this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
        result => {
          if (result.hasPermission) {

            //If having permission show 'Turn On GPS' dialogue
            this.askToTurnOnGPS();
          } else {

            //If not having permission ask for permission
            this.requestGPSPermission();
          }
        },
        err => {
          alert(err);
        }

      );

    this.disableButton = false;
    
     
  }

  //Request permission if none granted
   requestGPSPermission() {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if (canRequest) {
          console.log("4");
        } else {
          //Show 'GPS Permission Request' dialogue
          this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
            .then(
              () => {
                // call method to turn on GPS
                this.askToTurnOnGPS();
              },
              error => {
                //Show alert if user click on 'No Thanks'
                alert('requestPermission Error requesting location permissions ' + error)
              }
            );
        }
      });
    }
  //ask user to turn on
    askToTurnOnGPS() {
      this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
        () => {
          // When GPS Turned ON call method to get Accurate location coordinates
          this.getLocationCoordinates()
        },
        error => alert('Error requesting location permissions ' + JSON.stringify(error))
      );
    }
    // Methods to get device accurate coordinates using device GPS
    getLocationCoordinates() {
      this.geolocation.getCurrentPosition().then((resp) => {
        this.locationCoords.longitude = resp.coords.latitude;
        this.locationCoords.latitude = resp.coords.longitude;
        this.locationCoords.accuracy = resp.coords.accuracy;
        this.locationCoords.timestamp = resp.timestamp;
      }).catch((error) => {
        alert('Error getting location' + error);
      });
    }


  async  Permission(){
   // console.log('longitude:',this.longitude.value);
   
   if(this.longitude.value==""){

   let alert = this.alertCtrl.create({

   message:"Field is empty",
   buttons: ['OK']
   }).then(res=>{
   	res.present();
   });
  }
  else{
  	if(this.latitude.value==""){

   let alert = this.alertCtrl.create({

   message:"Field is empty",
   buttons: ['OK']
   }).then(res=>{
   	res.present();
   });
  }else{
  	if(this.accuracy.value==""){

   let alert = this.alertCtrl.create({

   message:"Field is empty",
   buttons: ['OK']
   }).then(res=>{
   	res.present();
   });
  }else{
  	if(this.timestamp.value==""){

   let alert = this.alertCtrl.create({

   message:"Field is empty",
   buttons: ['OK']
   }).then(res=>{
   	res.present();
   });
  // }else{
  //   if(this.fifty.value==""){
  //     let alert = this.alertCtrl.create({

  //  message:"Field is empty",
  //  buttons: ['OK']
  //  }).then(res=>{
  //    res.present();
  //  });
     }
  else{
  	var headers = new Headers();
      headers.append('Access-Control-Allow-Origin' , '*');
     headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
     headers.append('Accept','application/json');
     headers.append('content-type','application/json');
      let options = new RequestOptions({ headers: headers });


        let data = {
          
          longitude: this.longitude.value,
          latitude: this.latitude.value,
          accuracy: this.accuracy.value,
          timestamp: this.timestamp.value
          
        };

        

   const loader =await this.loadingctrl.create({
      message: 'Loading.. Wait!!',
    });

   loader.present().then(() => {


   // this.http.post('http://127.0.0.1/Aud/AudDiary/gps.php',data,options)
   // // .map(res => res.json())
   //  .subscribe(res => {
   //  console.log(res)
   //  loader.dismiss()
   //  // if(data=="Location Found"){
     
   // // //   // let alert= this.alertCtrl.create({
   // // //   //    message:"Welcome..",
   // // //   //    buttons: ['OK']
   // // //   //    }).then(result=>{
   // // //   //    	 result.present();
   // // //   //    });
   //     //TODO: i don't know the structure of your data, i'm assuming this is the GPS ID you want to pass to the selector page
   //      //i was not fully able to test this but it should work
   //      let gpsId = res;
       
   //      //all this is doing is pushing the data into the browser history object. you can now reference it in the next page
   //      this.router.navigateByUrl('selector',{state: {res: {gpsId}}});
   // //      this.router.navigateByUrl('selector');
   // //  }else
   // //  {
   // //   let alert = this.alertCtrl.create({
   // //   message:"ERROR",
   // //   buttons: ['OK']
   // //   }).then(result=>{
   // //   	result.present();
   // //   });
   //    // } 
   //  });


  //external
  this.http.post('http://166.62.28.93/Aud/AudDiary/gps.php',data,options)
  //   .map(res => res.json())
    .subscribe((res) => {
    console.log(res)
    loader.dismiss()
    // if(data=="Location Found"){
     
     // let alert= this.alertCtrl.create({
     //    message:"Welcome..",
     //    buttons: ['OK']
     //    }).then(result=>{
     //    	 result.present();
     //    });
     //TODO: i don't know the structure of your data, i'm assuming this is the GPS ID you want to pass to the selector page
        //i was not fully able to test this but it should work
        let gpsId = res;
       
        //all this is doing is pushing the data into the browser history object. you can now reference it in the next page
        this.router.navigateByUrl('selector',{state: {res: {gpsId}}});
       
    // }else
    // {
    //  let alert = this.alertCtrl.create({
    //  message:"ERROR",
    //  buttons: ['OK']
    //  }).then(result=>{
    //  	result.present();
    //  });
    //   } 
    });



    });
  }
  }
  }
  }

  }




  }

    
