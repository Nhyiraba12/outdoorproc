// import { MbscModule } from '@mobiscroll/angular';
import { NgModule,ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import {HttpModule} from '@angular/http';
import { HomePage } from '../app/pages/home/home.page';
import { LocationPage } from '../app/pages/location/location.page';
import { VerifyPage } from '../app/pages/verify/verify.page';
import { SlidePage } from '../app/pages/slide/slide.page';
import { CameraPage } from '../app/pages/camera/camera.page';
import { PreviewAnyFile } from '@ionic-native/preview-any-file/ngx';
import {MediaCapture, MediaFile, CaptureError, CaptureImageOptions } from '@ionic-native/media-capture/ngx';
//Camera imports
import {IonicStorageModule} from '@ionic/storage';
import {HttpClientModule} from '@angular/common/http';
import {Camera} from '@ionic-native/Camera/ngx';
import {File,FileEntry} from '@ionic-native/File/ngx';
import {WebView} from '@ionic-native/ionic-webview/ngx';
// import {HttpClient} from '@angular/common/http';
import {HttpHandler} from '@angular/common/http';
// import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
//GPS imports
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import {Plugins} from '@capacitor/core';
import { GlobalService } from "../app/global.service";
import {HTTP} from '@ionic-native/http/ngx';

@NgModule({
  declarations: [AppComponent,HomePage,LocationPage,SlidePage,CameraPage,VerifyPage],
  entryComponents: [HomePage,LocationPage,SlidePage,CameraPage,VerifyPage],
  imports: [  BrowserModule,FormsModule,HttpClientModule, 
   IonicModule.forRoot(), AppRoutingModule,
   IonicStorageModule.forRoot(),HttpModule],
  providers: [
    StatusBar,
    Camera,
    File,FileTransfer,
    FilePath,
    WebView,
    HTTP,
    HttpClientModule,
    PreviewAnyFile,
    MediaCapture,
   GlobalService,

    // FileTransfer, 
    // FileTransferObject,
    // FilePath,
    AndroidPermissions,
    Geolocation,
    LocationAccuracy,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
