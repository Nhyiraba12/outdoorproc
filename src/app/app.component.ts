import { Component,OnInit,ViewChild } from '@angular/core';

import { Platform } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import {Storage} from '@ionic/storage';
import {NavController} from '@ionic/angular';
import { StartPage } from '../app/pages/start/start.page';
import { SlidePage } from '../app/pages/slide/slide.page';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  @ViewChild('content') nav:NavController;
rootPage:any;
  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    public router:Router,
    private storage:Storage,
  ) {
    this.initializeApp();
  }

  initializeApp() {
      this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.router.navigateByUrl('start');
    });


//needed to navigate to app home
   this.storage.get('session_store').then((res)=>{
     if(res == null){
          this.rootPage=SlidePage;
     }else{
       this.rootPage=StartPage;
     }
   });
  }
}
