import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { PlacePageRoutingModule } from './place-routing.module';

import { PlacePage } from './place.page';
import { NgSelectModule } from '@ng-select/ng-select';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
     ReactiveFormsModule,
    IonicModule,
    PlacePageRoutingModule,
     RouterModule.forChild([
      {
        path: '',
        component: PlacePage
      }
    ]),
      NgSelectModule
  ],
  declarations: [PlacePage]
})
export class PlacePageModule {}
