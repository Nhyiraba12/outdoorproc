import { Component,OnInit,ViewChild,ElementRef } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {AlertController} from '@ionic/angular';
import {LoadingController} from '@ionic/angular';
import { VerifyPage } from '../pages/verify/verify.page';
import {Http, Headers, RequestOptions}  from "@angular/http";
import 'rxjs/add/operator/map';
import {IonSearchbar} from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { TypeaheadService } from '../services/typeahead.service';

@Component({
  selector: 'app-place',
  templateUrl: './place.page.html',
  styleUrls: ['./place.page.scss'],
})
export class PlacePage implements OnInit {
	@ViewChild("place") place:ElementRef;
  //search child
  @ViewChild('search',{static :false}) search :IonSearchbar;
  // @ViewChild("name") name;

    //search items
  public list :Array<object>= [];
  private searchedItem:any;

  showList: boolean = false;
  searchQuery: string = '';
 
	data:string;
	items:any;


// form: FormGroup;
//   states: Observable<string[]>;
//   selectedCityNames: string[];
//   cities2 = [
//     { id: 1, name: 'Vilnius' },
//     { id: 2, name: 'Kaunas' },
//     { id: 3, name: 'Pavilnys', disabled: true },
//     { id: 4, name: 'Pabradė' },
//     { id: 5, name: 'Klaipėda' }
//   ];
  constructor(public navCtrl:NavController, private http: Http,private formBuilder: FormBuilder,private typeahead: TypeaheadService,private router:Router,private loadingctrl:LoadingController,private alertCtrl:AlertController) { 
 // this.createForm();

  
    // this.list=[

    //   {title:"Kasoa"},
    //   {title:"Osu-oxford Street"},
    //   {title:"Legon"},
    //   {title:"Dzorwulu"},
    //   {title:"Achimota"},

    // ];
    // this.searchedItem=this.list

  }
// createForm() {
//     this.form = this.formBuilder.group({
//       name: [],
//     });
//   }

//   getStatesName(event) {
//     this.states = this.typeahead.getStates(event.target.value);
//   }
 ngAfterViewInit() {
    var input = document.getElementById('place').getElementsByTagName('input')[0];
    // var options = {componentRestrictions: {country: 'us'}};
    // new google.maps.places.Autocomplete(input, options);
}

  ngOnInit() {
  }

ionViewDidEnter(){
  // setTimeout(()=>{
  //   this.search.setFocus();
  // });
}
//search script
  _ionChange(event){
    
    // console.log(event.target.value);
    const val = event.target.value;
    this.searchedItem = this.list;
    if(val && val.trim() != ''){
      this.searchedItem =this.searchedItem.filter((item:any)=>{
        return(item.title.toLowerCase().indexOf(val.toLowerCase())> -1);
      })
     
      }
  }

async Adloc(){

console.log('place:',this.place.nativeElement.value);
 if(this.place.nativeElement.value==""){

 let alert = this.alertCtrl.create({

 message:"Field is empty",
 buttons: ['OK']
 }).then(res=>{
 	res.present();
 });
}
 else
 {

  var headers = new Headers();
   headers.append('Access-Control-Allow-Origin' , '*');
     headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
     headers.append('Accept','application/json');
     headers.append('content-type','application/json');
    let options = new RequestOptions({ headers: headers });


      let data = {
        place: this.place.nativeElement.value
      };

      

 const loader =await this.loadingctrl.create({
    message: 'Loading....!!',
  });

 loader.present().then(() => {


 // this.http.post('http://127.0.0.1/Aud/AudDiary/locate.php',data,options)
 //  .map(res => res.json())
 //  .subscribe(res => {
 //  console.log(res)
 //  loader.dismiss()

  
 //  // if(res=="Login successful"){
   
 //   // let alert= this.alertCtrl.create({
 //   //    message:"Welcome..Enter Location",
 //   //    buttons: ['OK']
 //   //    }).then(result=>{
 //   //    	 result.present();
 //   //    });
     
 //      this.router.navigateByUrl('gps');
 //  // }else
 //  // {
 //  //  let alert = this.alertCtrl.create({
 //  //  message:"ERROR",
 //  //  buttons: ['OK']
 //  //  }).then(result=>{
 //  //  	result.present();
 //  //  });
 //  //   } 
 //  });


//external
this.http.post('http://166.62.28.93/Aud/AudDiary/locate.php',data,options)
  // .map(res => res.json())
  .subscribe(res => {
  console.log(res)
  loader.dismiss()
//   if(res=="Location Selected"){
   
//    let alert= this.alertCtrl.create({
//       message:"Welcome..",
//       buttons: ['OK']
//       }).then(result=>{
//       	 result.present();
//       });
     
      this.router.navigateByUrl('gps');
//   }else
//   {
//    let alert = this.alertCtrl.create({
//    message:"ERROR",
//    buttons: ['OK']
//    }).then(result=>{
//    	result.present();
//    });
//     } 
  });



  });
 
   }
  
  }



}












	