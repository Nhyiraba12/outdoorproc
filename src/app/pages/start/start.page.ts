import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-start',
  templateUrl: './start.page.html',
  styleUrls: ['./start.page.scss'],
})
export class StartPage implements OnInit {
  splash = true;
  // secondPage = SecondPagePage;
  constructor(public router:Router,public loadingCtrl:LoadingController) { 
    setTimeout(()=>{
      this.router.navigateByUrl('slide');
    this.splash=false},5000);
  }

  ngOnInit() {
  }

}
