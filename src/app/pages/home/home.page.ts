import { Component,OnInit,ViewChild } from '@angular/core';
import { NavController } from '@ionic/angular';

import { CameraPage } from '../../pages/camera/camera.page';

import { LoadingController } from '@ionic/angular';

import {Http, Headers, RequestOptions}  from '@angular/http';

import 'rxjs/add/operator/map';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
@ViewChild("longitude") longitude;
@ViewChild("latitude") latitude;
@ViewChild("accuracy") accuracy;
@ViewChild("timestamp") timestamp;
@ViewChild("img") img;

images:any;


  constructor(private loadingctrl:LoadingController) {}


 ngOnInit(){

}


}