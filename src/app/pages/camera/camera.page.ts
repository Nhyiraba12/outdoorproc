    import { Component, OnInit,ChangeDetectorRef,ViewChild,ElementRef } from '@angular/core';
    import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
    import { Geolocation } from '@ionic-native/geolocation/ngx';
    import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
    import {Router,ActivatedRoute} from '@angular/router';
    import {ActionSheetController,ToastController,Platform} from '@ionic/angular';
    import {Http, Headers, RequestOptions}  from "@angular/http";
    import {AlertController} from '@ionic/angular';
    import {LoadingController} from '@ionic/angular';
    import {Camera,CameraOptions,PictureSourceType} from '@ionic-native/Camera/ngx';
    import {File,FileEntry} from '@ionic-native/File/ngx';
   
    import {GpsPage} from '../../gps/gps.page';
    // import {HttpClient} from '@angular/common/http';
    import {WebView} from '@ionic-native/ionic-webview/ngx';
    import {Storage} from '@ionic/storage';
    import { finalize } from 'rxjs/operators';
    import {HttpHandler} from '@angular/common/http';
    import { Plugins, FilesystemDirectory, FilesystemEncoding } from '@capacitor/core';
    import { FilePath } from '@ionic-native/file-path/ngx';
    import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
    import * as watermark from 'watermarkjs';
    import { Capacitor, CameraResultType} from '@capacitor/core';
    import { GlobalService } from "../../global.service";
    import { TypeaheadService } from '../../services/typeahead.service';
    import {SelectorPage} from '../../selector/selector.page';

    const STORAGE_KEY = 'xav_images';
    // declare var gpsId:'this.router.getCurrentNavigation().extras.state';
    
    const uploadURL = 'https://192.168.2.22/Aud/AudDiary/upload.php'; 
    const upploadURL = 'https://127.0.0.1/Aud/AudDiary/upload.php';
    @Component({
      selector: 'app-camera',
      templateUrl: './camera.page.html',
      styleUrls: ['./camera.page.scss'],
    })
    export class CameraPage implements OnInit {
    private result;
    
    @ViewChild("gpsId") gpsId;
    // @ViewChild("latitude") latitude;
    // @ViewChild("accuracy") accuracy;
    // @ViewChild("timestamp") timestamp;
    @ViewChild("gps_id") gps_id;
    //Call objects
    locationCoords:any;
    timetest:any;
    gpsid = this.tservice.getIdd().subscribe(data=>{
          console.log(data);
          this.result = data;
        
          // console.log(this.result);
        });
    // gpsId:any;
    images=[];
      constructor(private loadingctrl:LoadingController,private tservice:TypeaheadService,public global: GlobalService,private route:ActivatedRoute,private transfer:FileTransfer,private router:Router,private filePath:FilePath,private locationAccuracy:LocationAccuracy,private androidPermissions:AndroidPermissions,private geolocation: Geolocation,private webview:WebView,private alertCtrl:AlertController,private actionsheet:ActionSheetController,private plat:Platform,private storage:Storage,private file:File,private http:Http,private toastCtrl:ToastController,private loading:LoadingController,private camera:Camera,private ref:ChangeDetectorRef) { 
        this.gpsId =this.router.getCurrentNavigation().extras.state;
        // console.log(gpsId);

      }

   

      ngOnInit() {
        
        //retrieving data from service
        //   this.tservice.getIdd().subscribe(data=>{
        //   console.log(data);
        //   this.result = data;
        
        //   console.log( this.gps_id);
        // });




       
       this.plat.ready().then(()=>{
          this.loadStoredImages();
        });

        var options: CameraOptions = {
            quality: 100,
            
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
     
        this.camera.getPicture(options).then(imagePath => {
     
            // if (this.plat.is('android') && this.camera.PictureSourceType.PHOTOLIBRARY) {
            //     this.filePath.resolveNativePath(imagePath)
            //         .then(filePath => {
            //             let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            //             let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            //             this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            //         });
            // } else {
            //     var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            //     var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            //     this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            // }
             const fileTransfer: FileTransferObject = this.transfer.create();


    let temp_image =new Date().getTime()+'.'+ this.result+'.jpg';
        let options1: FileUploadOptions = {
          
           fileKey: 'file',
           httpMethod: 'POST',
           fileName: temp_image,
           headers: {}
        
        }
    const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    // fileTransfer.upload(imagePath, 'http://127.0.0.1/Aud/AudDiary/upload.php', options)
    //  .then((data) => {
    //    // success
    //    alert("Saved");
    //  }, (err) => {
    //    // error
    //    alert("error"+JSON.stringify(err));
    //  });

    // if(this.gpsId.value != ''){
    //    let alert = this.alertCtrl.create({

    //  message:this.gpsId.value,
    //  buttons: ['OK']
    //  }).then(res=>{
    //    res.present();
    //  });
    // }else{

    //   var headers = new Headers();
    //     headers.append("Accept", 'application/json');
    //     headers.append('Content-Type', 'application/json' );
    //     let requestoptions = new RequestOptions({ headers: headers });

    // // let gpsdata ={
    // //   gpsId : this.gpsId.value
    // // };

    // //GPSID POST HTTP
    // var gpsdata=this.gpsId;

    // this.http.get('http://192.168.2.22/Aud/AudDiary/gid.php',gpsdata)
    //  .map(res => res.json())
    //  .subscribe((res) => {
    //   console.log(res);
      


    
    
     // fileTransfer.upload(imagePath,'http://192.168.2.22/Aud/AudDiary/upload.php',options1)
     // .then((data) => {


     //  // console.log(link);
     //   // this.http.post('http://192.168.2.22/Aud/AudDiary/update.php',gps).subscribe(
     //  //     (response) => console.log(response),
     //  //     (error) => console.log(error)
     //  //  )
     //  // .subscribe((res) => {
     //  // console.log(res)

     //   // success
     //   // alert("Saved");
     // }, (err) => {
     //   // error
     //   alert("error"+JSON.stringify(err));
     // });
  // });
    // var link = 'http://192.168.2.22/Aud/AudDiary/upload.php';
      fileTransfer.upload(imagePath,'http://166.62.28.93/Aud/AudDiary/upload.php',options1)
     .then((data) => {
// 192.168.2.22

    
       // this.http.post('http://192.168.2.22/Aud/AudDiary/update.php',gps).subscribe(
      //     (response) => console.log(response),
      //     (error) => console.log(error)
      //  )
      // .subscribe((res) => {
      // console.log(res)

       // success
       // alert("Saved");
     }, (err) => {
       // error
       alert("error"+JSON.stringify(err));
     });
  // }

    // var link = 'http://192.168.2.22/Aud/AudDiary/upload.php';
    
    //  fileTransfer.upload(imagePath,link,options1)
    //  .then((data) => {


    //   // console.log(link);
    //    // this.http.post('http://192.168.2.22/Aud/AudDiary/update.php',gps).subscribe(
    //   //     (response) => console.log(response),
    //   //     (error) => console.log(error)
    //   //  )
    //   // .subscribe((res) => {
    //   // console.log(res)

    //    // success
    //    // alert("Saved");
    //  }, (err) => {
    //    // error
    //    alert("error"+JSON.stringify(err));
    //  });
    // });

      });
    //   var link = 'http://192.168.2.22/Aud/AudDiary/upload.php';//+this.tservice.getIdd().subscribe(data=>{
    //  // //      console.log(data);
    //  // //      this.result = data;
    //  // //    });
    //    console.log(link);
    // // console.log(this.gpsId);
    // var gpsId = history.state;
      }


    //load images unto page
     loadStoredImages(){
         this.storage.get(STORAGE_KEY).then(images => {
          if (images) {
            let arr = JSON.parse(images);
            this.images = [];
            for (let img of arr) {
              let filePath = this.file.dataDirectory + img;
              let resPath = this.pathForImage(filePath);
              this.images.push({ name: img, path: resPath, filePath: filePath });
            }
          }
        });
      }

      //stores filePath ,convert to displayable file
       pathForImage(img){
        if (img === null) {
          return '';
        } else {
          let converted = this.webview.convertFileSrc(img);
          return converted;
        }
      }


      async presentToast(text){
        const toast= await this.toastCtrl.create({
          message:text,
          position:'bottom',
          duration:3000
        });
        toast.present();
      }
      async select(){
        const sheet = await this.actionsheet.create({
          header:'Get Source Image',
            buttons:[//{
              //text:'Load From Gallery',handler:()=>{this.takePhoto(this.camera.PictureSourceType.PHOTOLIBRARY);
            // }
           //},
          {
            text:'Camera',handler:()=>{this.takePhoto(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text:'Cancel',
          role:'Cancel'
        }]

        });
        await sheet.present();
      }
      
      //takePhoto
       takePhoto(sourceType : PictureSourceType)
    //    const options: CameraOptions = {
    //         quality: 100,
    //         sourceType: sourceType,
    //         saveToPhotoAlbum: true,
    //         correctOrientation: true
    //     };
     
    //     this.camera.getPicture(options).then(imageData => {
    //     //     if (this.plat.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
    //     //         this.filePath.resolveNativePath(imageData)
    //     //             .then(filePath => {
    //     //                 const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
    //     //                 const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
    //     //                 this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    //     //             });
    //     //     } else {
    //     //         const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    //     //         const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    //     //         this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    //     //     }
    //     // });
    //  const fileTransfer: FileTransferObject = this.transfer.create();

    //     let options1: FileUploadOptions = {
    //        fileKey: 'file',
    //        fileName: 'name.jpg',
    //        headers: {}
        
    //     }

    // fileTransfer.upload(imageData, 'https://localhost/ionic/upload.php', options1)
    //  .then((data) => {
    //    // success
    //    alert("success");
    //  }, (err) => {
    //    // error
    //    alert("error"+JSON.stringify(err));
    //  }
    // });
    // }
     {
        
         let options = {

             quality: 100,
              sourceType: sourceType,
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
            saveToPhotoAlbum: false,
           correctOrientation: true
              };

        this.camera.getPicture(options).then((imagePath) => {
         // imageData is either a base64 encoded string or a file URI
         // If it's base64:

       const fileTransfer: FileTransferObject = this.transfer.create();

       let temp_image =new Date().getTime()+'.'+ this.result+'.jpg';
        let options1: FileUploadOptions = {
           fileKey: 'file',
           fileName:temp_image,
           headers: {}
        
        }
    const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
    const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
    // fileTransfer.upload(imagePath, 'http://127.0.0.1/Aud/AudDiary/upload.php', options1)
    //  .then((data) => {
    //    // success
    //    alert("success");
    //  }, (err) => {
    //    // error
    //    alert("error"+JSON.stringify(err));
    //  });
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });


         // let gpsId = history.state.res;
     fileTransfer.upload(imagePath,'http://166.62.28.93/Aud/AudDiary/upload.php',options1)
     .then((data) => {
    console.log(data);
      
       // this.http.post('http://192.168.2.22/Aud/AudDiary/update.php?gpsId=180',gps).subscribe(
       //    (response) => console.log(response),
       //    (error) => console.log(error)
       // )

       // success
       // alert("Saved");
     }, (err) => {
       // error
       alert("error"+JSON.stringify(err));
     });

      });


    }


     createFileName(){
        const d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
        return newFileName;
      }

    copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName)
        .then(success => {
            this.updateStoredImages(newFileName);

             this.presentToast('Success while storing file.');
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }



      updateStoredImages(name){
       this.storage.get(STORAGE_KEY).then(images => {
            let arr =[];
            if(images &&  images !== '' && images.length>0){
              arr =  JSON.parse(images);
            }else{
              arr=[];
            }
           
            if (!arr) {
                const newImages = [name];
                this.storage.set('xav_images', JSON.stringify(newImages));
            } else {
                arr.push(name);
                this.storage.set('xav_images', JSON.stringify(arr));
            }
     
            const filePath = this.file.dataDirectory + name;
            const resPath = this.pathForImage(filePath);
     
            const newEntry = {
                name: name,
                path: resPath,
                filePath: filePath
            };
     
            this.images = [newEntry, ...this.images];
            this.ref.detectChanges(); // trigger change detection cycle
        });
        
    }

    async Remove(imgEntry,position){
       let confirm =await this.alertCtrl.create({
            message: 'Sure you want to delete this photo? Cannot be undone!!',
            buttons: [
              {
                text: 'No',
                handler: () => {
                  console.log('Disagree clicked');
                }
              }, {
                text: 'Yes',
                handler: () => {
                  console.log('Agree clicked');
                  // this.photos.splice(imgEntry, 1);
                  this.images.splice(position,1);
      this.storage.get(STORAGE_KEY).then(images=>{
        let arr = JSON.parse(images);
        let filtered = arr.filter(name=>name != imgEntry.name);
        this.storage.set(STORAGE_KEY,JSON.stringify(filtered));

        var correctPath = imgEntry.filePath.substr(0,imgEntry.filePath.lastIndexOf('/')+1);

        this.file.removeFile(correctPath,imgEntry.name).then(res=>{
          this.presentToast('File deleted');
        });
      });
                }
              }
            ]
          });
        confirm.present();
      
    }

    // //Upload Function
    // Upload(imgEntry,position){
    //  this.file.resolveLocalFilesystemUrl(imgEntry.filePath).then(entry => {
    //             ( <FileEntry>entry).file(file => 
    //               this.readFile(file,imgEntry,position));
    //         })
    //                 .catch(err => {
    //             this.presentToast('Error while reading file.');
    //         });
    // }


    // //read snapped file
    // readFile(file:any,imgEntry,position){
    //   const reader = new FileReader();
    //     reader.onload = () => {
    //         const formData = new FormData();
    //         const imgBlob = new Blob([reader.result], {
    //             type: file.type
    //         });
    //         formData.append('file', imgBlob, file.name);
    //         this.uploadImageData(formData,imgEntry,position);
    //     };
    //     reader.readAsArrayBuffer(file);
    //   }




    

    // //uploadImageData
    //   async uploadImageData(formData:FormData,imgEntry,position){
    //    const loading = await this.loadingctrl.create({
    //         message: 'Uploading image...',
    //     });
    //     await loading.present();
     
    //     this.http.post('http://127.0.0.1/Aud/AudDiary/upload.php', formData)
    //         .pipe(
    //             finalize(() => {
    //                 loading.dismiss();
    //             })
    //         )
    //         .subscribe(res => {
    //             if (res['success']) {
    //                 this.presentToast('File upload complete.')
    //             } else {
    //                 this.presentToast('File upload failed.')
    //             }
    //         });
    //         this.http.post('http://192.168.2.33/Aud/AudDiary/upload.php', formData)
    //         .pipe(
    //             finalize(() => {
    //                 loading.dismiss();
    //             })
    //         )
    //         .subscribe(res => {
    //             if (res['success']) {
    //                 this.presentToast('File upload complete.')
    //             } else {
    //                 this.presentToast('File upload failed.')
    //             }
    //         });
    //   }



    //Check if permission has been granted
    // checkGPSPermission(){
    //   this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then(
    //       result => {
    //         if (result.hasPermission) {

    //           //If having permission show 'Turn On GPS' dialogue
    //           this.askToTurnOnGPS();
    //         } else {

    //           //If not having permission ask for permission
    //           this.requestGPSPermission();
    //         }
    //       },
    //       err => {
    //         alert(err);
    //       }

    //     );


       
    // }

    // //Request permission if none granted
    //  requestGPSPermission() {
    //     this.locationAccuracy.canRequest().then((canRequest: boolean) => {
    //       if (canRequest) {
    //         console.log("4");
    //       } else {
    //         //Show 'GPS Permission Request' dialogue
    //         this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
    //           .then(
    //             () => {
    //               // call method to turn on GPS
    //               this.askToTurnOnGPS();
    //             },
    //             error => {
    //               //Show alert if user click on 'No Thanks'
    //               alert('requestPermission Error requesting location permissions ' + error)
    //             }
    //           );
    //       }
    //     });
    //   }
    // //ask user to turn on
    //   askToTurnOnGPS() {
    //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
    //       () => {
    //         // When GPS Turned ON call method to get Accurate location coordinates
    //         this.getLocationCoordinates()
    //       },
    //       error => alert('Error requesting location permissions ' + JSON.stringify(error))
    //     );
    //   }
    //   // Methods to get device accurate coordinates using device GPS
    //   getLocationCoordinates() {
    //     this.geolocation.getCurrentPosition().then((resp) => {
    //       this.locationCoords.latitude = resp.coords.latitude;
    //       this.locationCoords.longitude = resp.coords.longitude;
    //       this.locationCoords.accuracy = resp.coords.accuracy;
    //       this.locationCoords.timestamp = resp.timestamp;
    //     }).catch((error) => {
    //       alert('Error getting location' + error);
    //     });
    //   }


      async Permission(){
       
      if(this.gps_id?.value==""){

     let alert = this.alertCtrl.create({

     message:"Field is empty",
     buttons: ['OK']
     }).then(res=>{
       res.present();
     }); 
   }
     else{
      var headers = new Headers();
       headers.append('Access-Control-Allow-Origin' , '*');
     headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
     headers.append('Accept','application/json');
     headers.append('content-type','application/json');
        let options = new RequestOptions({ headers: headers });


          let data = {
            // longitude: this.longitude?.value,
            // latitude: this.latitude?.value,
            // accuracy: this.accuracy?.value,
            // timestamp: this.timestamp?.value,
            gps_id:this.gps_id?.value
          };

        const loader =await this.loadingctrl.create({
        message: 'Files Saved...Take upclose!!',
      });

     loader.present().then(() => {
      //external
    this.http.post('http://166.62.28.93/Aud/AudDiary/update.php',data,options)
      // .map(res => res.json())
      .subscribe(res => {
      console.log(res)
      loader.dismiss()
      // if(res=="Upload Successful"){
       
      //  let alert= this.alertCtrl.create({
      //     message:"Upload Successful",
      //     buttons: ['OK']
      //     }).then(result=>{
      //        result.present();
      //     });
         

          this.router.navigateByUrl('selector');
        
      // }else
      // {
      //  let alert = this.alertCtrl.create({
      //  message:"ERROR",
      //  buttons: ['OK']
      //  }).then(result=>{
      //    result.present();
      //  });
      //   } 
      });

    //  // console.log('longitude:',this.longitude.value);

    //  if(this.img?.value==""){

    //  let alert = this.alertCtrl.create({

    //  message:"Field is empty",
    //  buttons: ['OK']
    //  }).then(res=>{
    //    res.present();
    //  }); }else{
    //   var headers = new Headers();
    //     headers.append("Accept", 'application/json');
    //     headers.append('Content-Type', 'application/json' );
    //     let options = new RequestOptions({ headers: headers });


    //       let data = {
    //         // longitude: this.longitude?.value,
    //         // latitude: this.latitude?.value,
    //         // accuracy: this.accuracy?.value,
    //         // timestamp: this.timestamp?.value,
    //         img:this.img?.value
    //       };

          

    //  const loader =await this.loadingctrl.create({
    //     message: 'File Uploading Based on Position,delete and reupload for all files!!',
    //   });

    //  loader.present().then(() => {


    //  this.http.post('http://127.0.0.1/Aud/AudDiary/gps.php',data,options)
    //   .map(res => res.json())
    //   .subscribe(res => {
    //   console.log(res)
    //   loader.dismiss()
    //   if(res=="Upload Successful"){
       
    //    let alert= this.alertCtrl.create({
    //       message:"Upload Successful",
    //       buttons: ['OK']
    //       }).then(result=>{
    //          result.present();
    //       });
         
    //       this.router.navigateByUrl('selector');
    //   }else
    //   {
    //    let alert = this.alertCtrl.create({
    //    message:"ERROR",
    //    buttons: ['OK']
    //    }).then(result=>{
    //      result.present();
    //    });
    //     } 
    //   });


    // //external
    // this.http.post('http://192.168.2.33/Aud/AudDiary/gps.php',data,options)
    //   .map(res => res.json())
    //   .subscribe(res => {
    //   console.log(res)
    //   loader.dismiss()
    //   if(res=="Upload Successful"){
       
    //    let alert= this.alertCtrl.create({
    //       message:"Upload Successful",
    //       buttons: ['OK']
    //       }).then(result=>{
    //          result.present();
    //       });
         
    //       this.router.navigateByUrl('selector');
    //   }else
    //   {
    //    let alert = this.alertCtrl.create({
    //    message:"ERROR",
    //    buttons: ['OK']
    //    }).then(result=>{
    //      result.present();
    //    });
    //     } 
    //   });
    });
    
 
}

}





    }

      
