import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { VerifyPage } from '../../pages/verify/verify.page';
import {Platform} from '@ionic/angular';
@Component({
  selector: 'app-slide',
  templateUrl: './slide.page.html',
  styleUrls: ['./slide.page.scss'],
})
export class SlidePage implements OnInit {
  subscribe:any;
  constructor(public router:Router,private platform:Platform,public loadingCtrl:LoadingController) {

    //exiting app //control
  this.subscribe = this.platform.backButton.subscribeWithPriority(88,()=>{
    if(this.constructor.name == "SlidePage" ){
      if(window.confirm("Are you sure you want to exit app?")){
        navigator["app"].exitApp();
      }
    }
  })
//exit //end


   }

  ngOnInit() {
  }

  async log(){
    const loading=await this.loadingCtrl.create({
      message:'mediaWatch',
      spinner:'circles',
      duration:2200
    });
    setTimeout(()=>{
      this.router.navigateByUrl('verify');
    },2200);
    await loading.present();
  }
async move(){
  const loading=await this.loadingCtrl.create({
    message:'mediaWatch',
    spinner:'circles',
    duration:2200
  });
  setTimeout(()=>{
    this.router.navigateByUrl('verify');
  },2200);
  await loading.present();
}
}

