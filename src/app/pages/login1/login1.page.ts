import { Component, OnInit } from '@angular/core';
import {Platform} from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-login1',
  templateUrl: './login1.page.html',
  styleUrls: ['./login1.page.scss'],
})
export class Login1Page implements OnInit {

  constructor(public router:Router,public platform:Platform,public loadingCtrl:LoadingController) { 

  }

  ngOnInit() {
  }
  
  async launchLocationPage(){
    const loading = await this .loadingCtrl.create({
      message:'mediaWatch',
      spinner:'circles',
      duration:2200
    });
    setTimeout(()=>{
      this.router.navigateByUrl('camera');
    },2200);
    await loading.present();
  }
  
  async loggg(){
    const loading = await this .loadingCtrl.create({
      message:'mediaWatch',
      spinner:'circles',
      duration:2200
    });
    setTimeout(()=>{
      this.router.navigateByUrl('login2');
    },2200);
    await loading.present();
  }
}
