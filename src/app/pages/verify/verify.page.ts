import { Component,OnInit,ViewChild } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {NavController} from '@ionic/angular';
import {AlertController} from '@ionic/angular';
import {LoadingController} from '@ionic/angular';
import { LocationPage } from '../../pages/location/location.page';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {HTTP} from '@ionic-native/http/ngx';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.page.html',
  styleUrls: ['./verify.page.scss'],
})
export class VerifyPage implements OnInit {
	@ViewChild("pincode") pincode;
	//@ViewChild("name") name;
	data:string;
	items:any;

  constructor(public navCtrl:NavController, private http: Http,private router:Router,private loadingctrl:LoadingController,private alertCtrl:AlertController) { }


  ngOnInit() {
  }

async Adlogin(){
 

 if(this.pincode.value==""){

 let alert = this.alertCtrl.create({

 message:"Field is empty",
 buttons: ['OK']
 }).then(res=>{
 	res.present();
 });
}
 else
 {

  var headers = new Headers();
    headers.append('Access-Control-Allow-Origin' , '*');
     headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
     headers.append('Accept','application/json');
     headers.append('content-type','application/json');
    let options = new RequestOptions({ headers: headers });


      let data = {
        pincode: this.pincode.value
      };

      

 const loader =await this.loadingctrl.create({
    message: 'Loading....',
  });

 loader.present().then(() => {


 // this.http.post('http://127.0.0.1/Aud/AudDiary/login.php',data,options)
 //  .map(res => res.json())
 //  .subscribe(res => {
 //  console.log(res)
 //  loader.dismiss()
 //  if(res=="Login successful"){
   
 //   let alert= this.alertCtrl.create({
 //      message:"Welcome..Enter Location",
 //      buttons: ['OK']
 //      }).then(result=>{
 //      	 result.present();
 //      });
     
 //      this.router.navigateByUrl('place');
 //  }else
 //  {
 //   let alert = this.alertCtrl.create({
 //   message:"ERROR",
 //   buttons: ['OK']
 //   }).then(result=>{
 //   	result.present();
 //   });
 //    } 
 //  });


//external
this.http.post('https://166.62.28.93/Aud/AudDiary/login.php',data,options)
  .map(res => res.json())
 .subscribe(res => {
  console.log(res)
  loader.dismiss()
  if("Login successful"){
   
   // let alert= this.alertCtrl.create({
   //    // message:"Welcome..Enter Location",
   //    // buttons: ['OK']
   //    }).then(result=>{
   //    	 result.present();
   //    });
     
      this.router.navigateByUrl('place');
  }else
  {
   let alert = this.alertCtrl.create({
   message:"ERROR",
   buttons: ['OK']
   }).then(result=>{
   	result.present();
   });
    } 
  });



  });
 
   }
  
  }



}












	