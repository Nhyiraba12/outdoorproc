import { Component, OnInit,ViewChild,Injectable,ElementRef } from '@angular/core';
import {Http, Headers, RequestOptions}  from "@angular/http";
import {NavController,NavParams} from '@ionic/angular';
import {AlertController} from '@ionic/angular';
import {LoadingController} from '@ionic/angular';
import { CameraPage } from '../../pages/camera/camera.page';
import { VerifyPage } from '../../pages/verify/verify.page';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {Geolocation} from '@ionic-native/geolocation/ngx';
import 'rxjs/add/operator/map';

declare var google:any;
@Component({
  selector: 'app-location',
  templateUrl: './location.page.html',
  styleUrls: ['./location.page.scss'],
})

@Injectable()

export class LocationPage implements OnInit {
	//@ViewChild('map',{static:true}) mapRef:ElementRef;
	//Location add to database
	@ViewChild("place") place;
	data:string;
	items:any;
	// latitude:any;
	// longitude:any;
	// public username={name:''};

	// public static lat;
	// public static lon;

	


  constructor(public navCtrl: NavController,public geol:Geolocation,public router:Router,public alertCtrl:AlertController,  private http: Http,  public loading: LoadingController) { 
  	// this.getGeolocation();
  }

 ngOnInit() {  
  }
  //GPS Get Places
// getGeolocation(){
// 	this.geol.getCurrentPosition().then((resp)=>{
// 		if(resp){
// 			this.latitude=resp.coords.latitude;
// 			this.longitude=resp.coords.longitude;
// 			this.showMap();
// 		}
// 	}).catch((error)=>{
// 		console.log('Error',error);
// 	});
// }
// showMap(){	
// 	const location = new google.maps.LatLng(this.latitude,this.longitude);
// 	var map =new google.maps.Map(document.getElementById('map'),{
// 		center:location,
// 		zoom:12,
// 		mapTypeId:'roadmap'
// 	});
// 	var input=document.getElementById('g-input');
// 	var searchBox=new google.maps.places.SearchBox(input);
// 	map.addListener('bounds_changed',function(){
// 		searchBox.setBounds(map.getBounds());
// 	});
// 	var markers=[];
// 	searchBox.addListener('places_changed',function(){
// 		var places = searchBox.getPlaces();
// 		if(places.length==0){
// 			return;
// 		}
// 		markers.forEach(function(marker){
// 			marker.setMap(null);
// 		});
// 		markers=[];
// 		var bounds = new google.maps.LatLngBounds();
// 		places.forEach(function(place){
// 			LocationPage.lat=place.geometry.viewport.Ya.i;
// 			LocationPage.lon=place.geometry.viewport.Ua.i;

// 			if(!place.geometry){
// 				console.log("None");
// 				return;
// 			}
// 			var icon={
// 				url:place.icon,
// 				size:new google.maps.Size(71,71),
// 				origin:new google.maps.Point(0,0),
// 				anchor:new google.maps.Point(17,34),
// 				scaledSize:new google.maps.Size(25,25)
// 			};
// 			markers.push(new google.maps.Marker({
// 				map:map,
// 				icon:icon,
// 				title:place.name,
// 				position:place.geometry.location
// 			}));
// 			if(place.geometry.viewport){
// 				bounds.union(place.geometry.viewport);
// 			}else{
// 				bounds.extend(place.geometry.location);
// 			}
// 		});
// 		map.fitBound(bounds);
// 	});
// }

ionViewDidLoad(){
	//this.getData();
}

// getData(){
// 	var url="'http://192.168.2.33:8100/Aud/AudDiary/person.php'";
// 	this.data = this.http.get(url);
// 	this.data.subscribe( data => {
//    		console.log(data);
//     }); 
// }



async Add(){
	console.log('place : ',this.place.value);

 if(this.place.value==""){

 let alert = this.alertCtrl.create({

 message:"Field is empty",
 buttons: ['OK']
 }).then(res=>{
 	res.present();
 });
}
 else
 {

  var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });


      let data = {
        place: this.place.value
      };

      

 const loader =await this.loading.create({
    message: 'Loading..',
  });

 loader.present().then(() => {


 this.http.post('http://127.0.0.1/Aud/AudDiary/locate.php',data,options)
  .map(res => res.json())
  .subscribe(res => {
  console.log(res)
  loader.dismiss()
  if(res=="Location Selected"){
   
   let alert= this.alertCtrl.create({
      message:"Welcome..",
      buttons: ['OK']
      }).then(result=>{
      	 result.present();
      });
     
      this.router.navigateByUrl('camera');
  }else
  {
   let alert = this.alertCtrl.create({
   message:"ERROR",
   buttons: ['OK']
   }).then(result=>{
   	result.present();
   });
    } 
  });


//external
this.http.post('http://192.168.2.22/Aud/AudDiary/locate.php',data,options)
  .map(res => res.json())
  .subscribe(res => {
  console.log(res)
  loader.dismiss()
  if(res=="Location Selected"){
   
   let alert= this.alertCtrl.create({
      message:"Welcome..",
      buttons: ['OK']
      }).then(result=>{
      	 result.present();
      });
     
      this.router.navigateByUrl('camera');
  }else
  {
   let alert = this.alertCtrl.create({
   message:"ERROR",
   buttons: ['OK']
   }).then(result=>{
   	result.present();
   });
    } 
  });



  });
 
   }
  
  }




}

    



